package org.randomproject.constructordemo;

import org.randomproject.constructordemo.dao.DemoDao;
import org.randomproject.constructordemo.dao.ComplexDemoDaoImpl;
import org.randomproject.constructordemo.dao.SimpleDemoDaoImpl;
import org.randomproject.constructordemo.service.RandomService;
import org.randomproject.constructordemo.service.RandomServiceImpl;

public class Main {
    public static void main(String[] args) {

        /*
        Declaración de la entidad que simulará ser guardada
         */
        DemoEntity demoEntity = new DemoEntity("my name", 99);

        /*
        Creación de una instancia de la clase SimpleDemoDaoImpl, que implementa DemoDao.
        La interfaz DemoDao es parte del constructor de la clase RandomServiceImpl,
        indicando que es un requisito para su creación.
        RandomServiceImpl implementa RandomService, interfaz que tiene el método "save",
        al cual invocamos.
         */
        DemoDao simpleDemoDao = new SimpleDemoDaoImpl();
        RandomService randomService1 = new RandomServiceImpl(simpleDemoDao);
        randomService1.save(demoEntity);

        /*
        Creación de una instancia de la clase ComplexDemoDaoImpl, la cual implementa DemoDao.
        Se puede reutilizar RandomServiceImpl, ya que su constructor espera una implementación
        de DemoDao.
        En un proyecto sería muy fácil cambiar de implementación sin afectar al resto del código.
         */
        DemoDao complexDemoDao = new ComplexDemoDaoImpl();
        RandomService randomService2 = new RandomServiceImpl(complexDemoDao);
        randomService2.save(demoEntity);
    }
}