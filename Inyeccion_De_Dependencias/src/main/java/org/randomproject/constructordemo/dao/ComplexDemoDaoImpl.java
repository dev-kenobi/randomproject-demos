package org.randomproject.constructordemo.dao;

import org.randomproject.constructordemo.DemoEntity;

public class ComplexDemoDaoImpl implements DemoDao {
    public boolean save(DemoEntity demoEntity) {
        System.out.println("ComplexDemoDaoImpl - Saved: " + demoEntity);
        return true;
    }
}