package org.randomproject.constructordemo.dao;

import org.randomproject.constructordemo.DemoEntity;

public class SimpleDemoDaoImpl implements DemoDao {
    public boolean save(DemoEntity demoEntity) {
        System.out.println("SimpleDemoDaoImpl - Saved");
        return true;
    }
}