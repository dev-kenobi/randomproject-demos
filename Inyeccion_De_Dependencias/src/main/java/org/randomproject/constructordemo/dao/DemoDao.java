package org.randomproject.constructordemo.dao;

import org.randomproject.constructordemo.DemoEntity;

public interface DemoDao {
    boolean save(DemoEntity demoEntity);
}
