package org.randomproject.constructordemo;

import lombok.Data;

@Data
public class DemoEntity {
    private String name;
    private int age;

    public DemoEntity(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "DemoEntity{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}