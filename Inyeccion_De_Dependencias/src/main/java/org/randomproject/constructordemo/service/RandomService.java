package org.randomproject.constructordemo.service;

import org.randomproject.constructordemo.DemoEntity;

public interface RandomService {
    boolean save (DemoEntity demoEntity);
}
