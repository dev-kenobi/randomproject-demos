package org.randomproject.constructordemo.service;

import org.randomproject.constructordemo.DemoEntity;
import org.randomproject.constructordemo.dao.DemoDao;

public class RandomServiceImpl implements RandomService {

    private final DemoDao demoDao;

    public RandomServiceImpl(DemoDao demoDao) {
        this.demoDao = demoDao;
    }

    public boolean save (DemoEntity demoEntity){
        return demoDao.save(demoEntity);
    }
}