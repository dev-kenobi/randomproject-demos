package org.randomproject.frameworkdemo.service;

public interface SpringWeapon {
    String shoot();
}
