package org.randomproject.frameworkdemo.service;

public interface Car {
    String move();
}
