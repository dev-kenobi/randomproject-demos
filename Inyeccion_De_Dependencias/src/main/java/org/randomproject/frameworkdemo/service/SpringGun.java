package org.randomproject.frameworkdemo.service;


import org.randomproject.setterdemo.service.Gun;
import org.springframework.stereotype.Service;

@Service
public class SpringGun implements SpringWeapon {


    public String shoot() {
        return "Gun shot";
    }
}
