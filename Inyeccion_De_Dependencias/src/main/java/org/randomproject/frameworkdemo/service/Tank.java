package org.randomproject.frameworkdemo.service;

import org.springframework.stereotype.Service;

@Service
public class Tank implements Car {
    public String move() {
        return "Going forward";
    }
}
