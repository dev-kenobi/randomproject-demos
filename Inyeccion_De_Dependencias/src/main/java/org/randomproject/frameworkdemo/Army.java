package org.randomproject.frameworkdemo;

import org.randomproject.frameworkdemo.service.Car;
import org.randomproject.frameworkdemo.service.SpringWeapon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Army {

    /*
    La interfaz Car está marcada como final; indicando que sólo puede asignarsele una
    instancia, una sola vez (en este caso en el constructor). Spring automáticamente
    escaneará las clases que implementan esta interfaz, creará una instancia de la clase
    elegida (en caso de varias implementaciones se puede indicar cual usar mediante una anotación)
    y la inyectará en tiempo de ejecución.

    Esta instancia no puede cambiarse.
     */
    private final Car car;

    /*
    La instancia de SpringWeapon se inyecta mediante setter, en caso de no encontrar
    una clase que implementa SpringWeapon, Spring lanzará un error impidiendo el inicio
    de la aplicación.

    Se utiliza la anotación @Autowired en el setter para indicarle a Spring que debe gestionar
    la instancia por este método; esto no impide que en otra parte de la aplicación se
    le asigne otra instancia u otra implementación.

    En versiones recientes de Spring no es necesaria esta anotación en el constructor.
     */
    private SpringWeapon springWeapon;

    public Army(Car car) {
        this.car = car;
    }

    @GetMapping("/constructor")
    public String constructor() {
        return car.move();
    }

    @GetMapping("/setter")
    public String setter() {
        return springWeapon.shoot();
    }

    @Autowired
    public void setSpringWeapon(SpringWeapon springWeapon) {
        this.springWeapon = springWeapon;
    }
}