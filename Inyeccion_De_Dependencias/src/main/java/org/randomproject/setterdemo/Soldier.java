package org.randomproject.setterdemo;

import lombok.Setter;
import org.randomproject.setterdemo.service.Weapon;

@Setter
public class Soldier {
    private Weapon weapon;

    public void shootTheEnemy() {
        if (weapon == null) {
            System.out.println("Without weapon");
        }
        else {
            weapon.shoot();
        }
    }
}