package org.randomproject.setterdemo;

import org.randomproject.setterdemo.service.Gun;
import org.randomproject.setterdemo.service.LaserGun;
import org.randomproject.setterdemo.service.Weapon;

public class Main {
    public static void main(String[] args) {

        /*
        Creación de una instancia de la clase Soldier, la cual podrá usar múltiples dependencias.
         */
        Soldier soldier = new Soldier();

        /*
        La clase Soldier internamente tiene como dependencia a la interfaz Weapon,
        la cual es usada en el método shootTheEnemy. Esta dependencia no es evidente
        a simple vista debido a que no ha sido declarada de forma explícita (en el constructor).

        Como la clase Soldier no tiene una implementación de la interfaz Weapon, tiene
        que hacer una validación y realizar una acción por defecto.
         */
        soldier.shootTheEnemy();

        /*
        Se crea una instancia de una clase que implementa la interfaz Weapon y se la asigna a la clase Soldier.
         */
        Weapon gun = new Gun();
        soldier.setWeapon(gun);
        soldier.shootTheEnemy();

        /*
        Se crea una instancia de otra clase que implementa la interfaz Weapon y se la asigna a la clase Soldier.
         */
        Weapon laserGun = new LaserGun();
        soldier.setWeapon(laserGun);
        soldier.shootTheEnemy();
    }
}