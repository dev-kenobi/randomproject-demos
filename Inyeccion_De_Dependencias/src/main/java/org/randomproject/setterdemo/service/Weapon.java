package org.randomproject.setterdemo.service;

public interface Weapon {
    void shoot();
}
