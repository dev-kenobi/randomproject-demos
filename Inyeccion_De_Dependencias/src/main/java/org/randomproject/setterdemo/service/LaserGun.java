package org.randomproject.setterdemo.service;

public class LaserGun implements Weapon {
    public void shoot() {
        System.out.println("Laser Gun shot");
    }
}