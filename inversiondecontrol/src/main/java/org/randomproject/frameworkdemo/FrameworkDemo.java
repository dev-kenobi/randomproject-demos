package org.randomproject.frameworkdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//Se crea un servicio rest usando una anotación de Spring.
//No sabemos en que momento se incializa esta clase,
//tampoco sabemos como o cuando fue agregada al container de Spring.
//No controlamos el flujo de ejecución cuando llega una petición,
//sólo sabemos que en algún punto se llamará al método customResponse.
@RestController
public class FrameworkDemo {

    @GetMapping("/frameworkdemo")
    public String customResponse() {
        return "https://blog.randomproject.org";
    }
}