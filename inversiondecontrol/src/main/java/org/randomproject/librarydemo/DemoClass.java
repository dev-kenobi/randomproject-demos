package org.randomproject.librarydemo;

import lombok.Data;

@Data
public class DemoClass {
    private String name;
    private int age;

    public DemoClass(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
