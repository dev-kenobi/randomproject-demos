package org.randomproject.librarydemo;

import com.google.gson.Gson;

public class LibraryDemo {

    private Gson gson;

    public LibraryDemo() {
        //Se crea una nueva instancia de la librería a usar
        this.gson = new Gson();
    }

    public static void main(String[] args) {
        //Se crea la clase a serializar con Gson
        DemoClass demoClass = new DemoClass("name", 0);

        LibraryDemo libraryDemo = new LibraryDemo();
        //Se invoca al método de la clase LibraryDemo
        libraryDemo.print(demoClass);
    }

    private void print(DemoClass demoClass) {
        //Invocamos la funcionalidad provista por la librería,
        //en este caso la serialización de una clase.
        //Se puede ver que en todo momento controlamos el flujo de ejecución
        System.out.print(gson.toJson(demoClass));
    }
}